﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Score_counter : MonoBehaviour
{

    private TextMeshProUGUI Score_text;
    private float count;
    public float valueToIncreaseEverySec;

    private void Awake()
    {
        Score_text = GetComponent<TextMeshProUGUI>();
    }
    private void Start()
    {
        count = 0;
        Score_text.text = "";
    }


    void Update()
    {
        count += valueToIncreaseEverySec * Time.deltaTime;
        //Debug.Log(Mathf.Round(count));
        int count_int = (int)count;
        Score_text.text = "Score: " + count_int.ToString();
    }
}
