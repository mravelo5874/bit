﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class block_mover : MonoBehaviour {

    public float block_speed;
    public float block_horizontal_speed;

    void Start ()
    {
		
	}
	
	void Update ()
    {
        transform.Translate(Vector3.back * block_speed * Time.deltaTime);

        float input_horizontal = Input.GetAxisRaw("Horizontal");
        float velocity_input = input_horizontal * block_horizontal_speed;

        transform.Translate(Vector2.right * velocity_input * Time.deltaTime);
    }
}
