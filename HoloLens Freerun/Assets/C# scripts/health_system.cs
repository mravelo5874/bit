﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class health_system : MonoBehaviour
{

    public int startingHealth = 10;                             // The amount of health the player starts the game with.
    private int currentHealth;                                   // The current health the player has.
    public Slider healthSlider;                                 // Reference to the UI's health bar.

    public TextMeshProUGUI Score_text;
    private float count;
    public float valueToIncreaseEverySec;

   // private bool game_over = false;
    private void Awake()
    {
        // Score_text = GetComponent<TextMeshProUGUI>();
        currentHealth = startingHealth;
        healthSlider.value = startingHealth;
        count = 0;
       // Score_text.text = "";
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Block"))
        {
            currentHealth--;
        }
    }
    void Update()
    {
        healthSlider.value = currentHealth;

        // while (game_over == false)
        // { 
        if (currentHealth == 0)
        {
              //  game_over = true;
        }

           // count += valueToIncreaseEverySec * Time.deltaTime;
          //  int count_int = (int)count;
           // Score_text.text = "Score: " + count_int.ToString();
        

        //}
        
    }
}
